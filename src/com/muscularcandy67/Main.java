package com.muscularcandy67;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static int age(String string) throws ParseException {   //Metodo statico per ritornare l'età di una persona da una Stringa
        SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("dd/MM/yyyy"); //Dichiaro un format per la data
        Calendar now = Calendar.getInstance(); //Istanzio la data di oggi
        Calendar dateOfBirth = Calendar.getInstance(); //Istanzio la data di nascita
        dateOfBirth.setTime(dateTimeFormatter.parse(string)); //Setto il valore della data di nascita al valore della string formattata in Date dal dateTimeFormatter
        if (dateOfBirth.after(now)) { //Controllo che non sia nato futuro
            throw new IllegalArgumentException("Non puoi essere nato nel futuro");
        }
        int year1 = now.get(Calendar.YEAR);
        int year2 = dateOfBirth.get(Calendar.YEAR);
        //Mi prendo i due anni e li sottraggo per l'età
        int age = year1 - year2;
        int month1 = now.get(Calendar.MONTH);
        int month2 = dateOfBirth.get(Calendar.MONTH);
        if (month2 > month1) { //Controllo se il mese della data di nascita è maggiore della data attuale, e nel caso decremento l'età per una maggiore correttezza
            age--;
        }
        else if (month1 == month2) { //Nel caso siano uguale effettuo lo stesso controllo precedente solo per ill giorno
            int day1 = now.get(Calendar.DAY_OF_MONTH);
            int day2 = dateOfBirth.get(Calendar.DAY_OF_MONTH);
            if (day2 > day1) {
                age--;
            }
        }
        return age;
    }

    public static void main(String[] args) throws ParseException {
        Date today = new Date(); //Data di oggi
        SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("dd/MM/yyyy"); //Un altro format di data per gg/mm/aaaa
        System.out.println(dateTimeFormatter.format(today)); //output della data con il metodo format
        Scanner keyboard = new Scanner(System.in);
        String input;
        do {
            System.out.println("Inserisci una data nel formato gg/mm/aaaa");
            input = keyboard.next();
        }while (!(input.matches("[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9]")));  //Richiedo la data fino a quando non è nel formato giusto. Ad esempio se inserisco 20/2/2018 non va bene mentre se inserisco 20/02/2018 va bene
        Date userInput = dateTimeFormatter.parse(input); //Istanzio la data inserita come Date copiando la data dalla String grazie al metodo parse
        String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ITALIAN).format(userInput); //Prendo la data della settimana usando il SimpleDateFormat con pattern "EEEE" per il ritorno del giorno, e la lingua italiana
        System.out.println(dayOfWeek);
        do{
            input=keyboard.next();
        }while (!(input.equalsIgnoreCase("20/4/2018"))); //Controllo che la data sia quella richiesta
        Date plusDate;
        plusDate = dateTimeFormatter.parse(input);
        Calendar cal = Calendar.getInstance();
        cal.setTime(plusDate);
        cal.add(Calendar.DATE, 45);
        plusDate = cal.getTime();
        System.out.println(dateTimeFormatter.format(plusDate));
        do {
            System.out.println("Inserisci una data nel formato gg/mm/aaaa");
            input = keyboard.next();
        }while (!(input.matches("[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9]")));
        System.out.println(age(input));
        do {
            System.out.println("Inserisci una data nel formato gg/mm/aaaa");
            input = keyboard.next();
        }while (!(input.matches("[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9]")));
        int age1 = age(input);
        do {
            System.out.println("Inserisci una data nel formato gg/mm/aaaa");
            input = keyboard.next();
        }while (!(input.matches("[0-9][0-9][/][0-9][0-9][/][0-9][0-9][0-9][0-9]")));
        int age2 = age(input);
        if(age1<age2) System.out.println("La prima persona è più giovane");
        else if(age1==age2) System.out.println("Hanno la stessa età");
        else if(age2<age1) System.out.println("La seconda persona è più giovane");
        else System.out.println("Qualcosa è andato storto");
    }
}